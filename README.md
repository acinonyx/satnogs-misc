# SatNOGS Miscellaneous

Various code and scripts for SatNOGS project.

More information can be found in our [documentation](https://docs.satnogs.org/).

## License

&copy; 2014-2020 [Libre Space Foundation](http://libre.space).

Licensed under the [GPLv3](LICENSE).
